
Theme:        Abaca (For Drupal 6.x)
Designed by:  Liz Y.
URL:          http://www.dropshipaccess.com

Notes:

This theme was designed to be used with a slogan (tag-line). 
To enable the slogan, go to Administer > Site building > Themes, 
find Abaca and click on configure.
Check the box for "Site slogan". 
Now go to Administer > Site information and enter your site slogan.

Abaca is 
XHTML 1.0 Strict and CSS Level 3 compliant.


2 column / 3 column control:

If you desire a 2 column layout, simply do not assign any blocks to the left
sidebar. The main content area will automatically expand to fill the empty
space.

You can use block visibility settings to control which pages have 2 columns
and which have 3. Simply set your left sidebar blocks not to display on paths
where you desire only 2 columns.
